from django.test import TestCase, LiveServerTestCase
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains

# Create your tests here.

class UnitTest(TestCase):
    
    def test_home_template(self):
        url = reverse('index')
        resp = self.client.get(url)
        self.assertTemplateUsed(resp, 'index.html')
    
    def test_home_response(self):
        url = reverse('index')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        self.selenium = webdriver.Firefox(firefox_options=options)
        super(FunctionalTest, self).setUp()
    
    def test_home_searchbox(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        searchbox = selenium.find_element_by_id("searchbox")
        searchboxButton = selenium.find_element_by_id("searchbutton")
        searchbox.sendKeys("How are you todie?")
        searchbox.click()
        selenium.implicitly_wait(10)
        assert 'Kosong' not in selenium.page_source


    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()