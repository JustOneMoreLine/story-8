from django.shortcuts import render
from django.http import HttpResponse
import json
import requests

# Create your views here.
url = 'https://www.googleapis.com/books/v1/volumes'
def index(request):
    return render(request, 'index.html')
def search(request):
    if request.method == 'GET':
        search = request.GET.get("data")
        data = requests.get(url, q=search)
        result = json.dumps(data)
        return HttpResponse(content=result, content_type="application/json")